﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundControlScript : MonoBehaviour {

    public AudioMixer masterMixer;

    public void SetMasterLvl(float lvl)
    {
        masterMixer.SetFloat("Master", lvl);
    }

    public void SetSfxLvl(float lvl)
    {
        masterMixer.SetFloat("Sound Effects", lvl);
    }

    public void SetMusicLvl(float lvl)
    {
        masterMixer.SetFloat("Music", lvl);
    }
}
