﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EntityScript : MonoBehaviour {
    public delegate void DestroyAction(Guid ID);
    public static event DestroyAction OnDeath;

    public Guid MyID { get; set; }
    public int health;
    public int attack;
    void Awake () {
        MyID = Guid.NewGuid();
        Debug.Log(string.Format("{0} has been assigned ID {1}", gameObject.name, MyID));
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        if (!otherObject.tag.Equals("Entity"))
            return;
        Rigidbody2D otherBody = otherObject.GetComponent<Rigidbody2D>();
        Rigidbody2D thisBody = this.GetComponent<Rigidbody2D>();
        if(thisBody.velocity.magnitude > otherBody.velocity.magnitude)
        {
            otherObject.GetComponent<EntityScript>().health -= this.GetComponent<EntityScript>().attack;
            GetComponent<AudioSource>().Play();
        }
        if(this.GetComponent<EntityScript>().health <= 0 && this.gameObject != null)
        {
            if(OnDeath != null)
            {
                OnDeath(this.MyID);
                Destroy(this.gameObject);
            }
            
        }
    }
}
