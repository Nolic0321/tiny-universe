﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GravityScript : MonoBehaviour
{
    public GameObject gravRingPrefab;

    Dictionary<Guid, GameObject> thingsInGravPull;
    Rigidbody2D planetRigidBody;
    const float GRAVIATIONAL_CONSTANT = .0006673f;
    float myMass;
    float myRadius;

    private void OnEnable()
    {
        EntityScript.OnDeath += RemovePlanetFromDictionary;
        GameScript.OnCreate += AddPlanetToDictionary;
        thingsInGravPull = new Dictionary<Guid, GameObject>();
    }

    void RemovePlanetFromDictionary(Guid ID)
    {
        if (thingsInGravPull.ContainsKey(ID))
            thingsInGravPull.Remove(ID);
    }

    void AddPlanetToDictionary(Guid ID,GameObject otherObject)
    {

        if (otherObject != null && otherObject != this.gameObject && otherObject.tag.Equals("Entity"))
        {
            thingsInGravPull.Add(ID, otherObject);
        }
    }

    private void OnDisable()
    {
        EntityScript.OnDeath -= RemovePlanetFromDictionary;
        GameScript.OnCreate -= AddPlanetToDictionary;
    }
    void Start()
    {
        AssignAndInstantiatePrivateVars();

    }

    private void AssignAndInstantiatePrivateVars()
    {
        planetRigidBody = GetComponent<Rigidbody2D>();
        myMass = this.gameObject.GetComponent<Rigidbody2D>().mass;
        myRadius = GetComponent<CircleCollider2D>().radius;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (KeyValuePair<Guid, GameObject> thing in thingsInGravPull)
        {
            GameObject thingObject = thing.Value;
            float pullFactor = FindMyPullFactor(thingObject.GetComponent<Rigidbody2D>().mass);
            Vector2 pullDirection = FindPullDirection(thingObject);
            thingObject.GetComponent<Rigidbody2D>().AddForce(pullDirection * pullFactor);
        }

    }

    float FindMyPullFactor(float otherMass)
    {
        return GRAVIATIONAL_CONSTANT * ((myMass * otherMass)/myRadius*myRadius);
    }

    private Vector2 FindPullDirection(GameObject otherPlanet)
    {

        return planetRigidBody.transform.position - otherPlanet.transform.position;
        //return planet.mass < otherPlanet.GetComponent<Rigidbody2D>().mass ? 0 : planet.transform.position - otherPlanet.transform.position;
    }
}
