﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugScript : MonoBehaviour {
    GameObject planet;
    public GameObject gravRingPrefab;
	// Use this for initialization
	void Start () {
        planet = gameObject;
        GameObject derp = Instantiate(gravRingPrefab) as GameObject;
        derp.transform.position = planet.transform.position;
        derp.transform.parent = planet.transform;
        derp.transform.localScale = planet.transform.localScale + new Vector3(2f, 2f, 2f);

	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(string.Format("{0} has been triggered by {1}", gameObject.name, collision.name));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log(string.Format("{0} has exited the trigger of {1}", collision.name, gameObject.name));
    }
}
