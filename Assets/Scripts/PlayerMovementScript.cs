﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {
    public float speed;
    private Rigidbody2D player;
    private const float SPEED_BOOST_MULTIPLIER = 10f;
    // Use this for initialization
    void Start() {
        player = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        MovementCheck();
    }

    private void MovementCheck()
    {
        float verticalMovement = Input.GetAxis("Vertical");
        float horizontalMovement = Input.GetAxis("Horizontal");

        Vector2 movement = new Vector2(horizontalMovement, verticalMovement);

        //TODO: Come back and check if we want instant stop or slowdown
        //if (movement == new Vector2(0.0f, 0.0f))
        //    player.velocity = new Vector2(0.0f, 0.0f);

        if (Input.GetButton("Jump"))
        {
            player.AddForce(movement * speed * SPEED_BOOST_MULTIPLIER);
        }
        else
        {
            player.AddForce(movement * speed);
        }
        
    }
}
