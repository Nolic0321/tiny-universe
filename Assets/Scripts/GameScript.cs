﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameScript : MonoBehaviour
{

    public delegate void PlanetCreatedAction(Guid ID,GameObject gameObject);
    public static event PlanetCreatedAction OnCreate;
    static float t = 0.0f;
    public GameObject planetPrefab;

    Vector3 cameraLL;
    Vector3 cameraLR;
    Vector3 cameraUL;
    Vector3 cameraUR;
    System.Random RNG = new System.Random();

    private const int MAIN_MENU_INDEX = 0;
    
    // Use this for initialization
    void Start()
    {
        t = 0.0f;
        cameraLL = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        cameraLR = Camera.main.ViewportToWorldPoint(new Vector3(1, 0));
        cameraUL = Camera.main.ViewportToWorldPoint(new Vector3(0, 1));
        cameraUR = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));

        SetUniverseBorders();
        StartCoroutine(SpawnPlanets());
    }

    private void SetUniverseBorders()
    {
        Vector3 cameraHeight = cameraUL - cameraLL;
        Vector3 cameraWidth = cameraLR - cameraLL;

        //Create Top/Bottom Objects
        GameObject top = new GameObject("topBorder");
        GameObject bottom = new GameObject("bottomBorder");

        //Create Top/Bottom Colliders
        BoxCollider2D topCollide = top.AddComponent<BoxCollider2D>();
        topCollide.size = new Vector2(cameraWidth.x, .05f);
        topCollide.sharedMaterial = (PhysicsMaterial2D)Resources.Load("PhysicsMaterial/Bouncy");
        BoxCollider2D bottomCollide = bottom.AddComponent<BoxCollider2D>();
        bottom.GetComponent<BoxCollider2D>().size = new Vector2(cameraWidth.x, .05f);
        bottomCollide.sharedMaterial = (PhysicsMaterial2D)Resources.Load("PhysicsMaterial/Bouncy");

        //Position Top/Bottom Colliders
        top.transform.position = new Vector3(0, cameraUL.y);
        bottom.transform.position = new Vector3(0, cameraLL.y);

        //Create Left/Right Objects
        GameObject left = new GameObject("leftBorder");
        GameObject right = new GameObject("rightBorder");

        //Create Left/Right Colliders
        BoxCollider2D collideLeft = left.AddComponent<BoxCollider2D>();
        collideLeft.size = new Vector2(.05f, cameraHeight.y);
        collideLeft.sharedMaterial = (PhysicsMaterial2D)Resources.Load("PhysicsMaterial/Bouncy");
        BoxCollider2D collideRight = right.AddComponent<BoxCollider2D>();
        collideRight.size = new Vector2(.05f, cameraHeight.y);
        collideRight.sharedMaterial = (PhysicsMaterial2D)Resources.Load("PhysicsMaterial/Bouncy");
        //Position Left/RightColliders
        left.transform.position = new Vector3(cameraLL.x, 0);
        right.transform.position = new Vector3(cameraLR.x, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Cancel")) { 
            if (SceneManager.GetActiveScene().buildIndex == 0)
                Application.Quit();
            GetComponent<LoadSceneScript>().LoadByIndex(MAIN_MENU_INDEX);
        }
    }

    private IEnumerator SpawnPlanets()
    {
        while (true)
        {
            GameObject newPlanet;
            float xPos = RNG.Next((int)cameraLL.x, (int)cameraLR.x);
            float yPos = RNG.Next((int)cameraLR.y, (int)cameraUR.y);
            newPlanet = Instantiate<GameObject>(planetPrefab,new Vector3(xPos,yPos), new Quaternion()) as GameObject;
            Guid planetGuid = newPlanet.GetComponent<EntityScript>().MyID;
            if(OnCreate != null)
            {
                OnCreate(planetGuid,newPlanet);
            }
            t += 0.03f * Time.timeSinceLevelLoad/60;
            yield return new WaitForSeconds(Mathf.Lerp(10f,.1f,t));
        }
    }
}
