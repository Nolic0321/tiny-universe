﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonstopMusicScript : MonoBehaviour {
    static bool audioBegin = false;
    private void Awake()
    {
        if (!audioBegin)
        {
            GetComponent<AudioSource>().Play();
            DontDestroyOnLoad(gameObject);
            audioBegin = true;
        }
    }
}
